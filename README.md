# Labor Days - Discover the days

## About

This library makes use of JavaScript, to find out which days
of the month for a given date aren't either Saturday or Sunday.

### Developer Installation

On the project's root dir, choose one:

* Custom NodeJS:
    - npm start
    - npm run test
    - npm run dev
* Docker in Linux:
    - sh runners/run-docker.sh
    - sh runners/run-dev-docker.sh
    - sh runners/run-test-docker.sh

### Requirements

- One of:
  - Node JS ^16.17.0
  - Docker ^20.10.12

## License

### MIT License

Copyright (c) <year>2022<copyright holders>
class DateHandler {
    constructor(newDate = new Date()) {
      this.newDate = newDate;
      this.laborDays = [1, 2, 3, 4, 5];
      this.currentMonthsLaborDays = [];
    }
  
    #createDateString(day) {
      const nd = new Date(
        this.newDate.getFullYear(),
        this.newDate.getDate(),
        day
      );
      return nd;
    }
  
    #getWeekDayIndex(day) {
      return this.#createDateString(day).getDay();
    }
  
    #getLastDayOfMonth(newDate) {
      return new Date(newDate.getFullYear(), newDate.getMonth(), 0).getDate();
    }
  
    #getCurrentMonthsLaborDays() {
      const monthsLaborDays = [];
      for (let day = 1; day < this.#getLastDayOfMonth(this.newDate); day++) {
        const todaysLaborDayIndex = this.#getWeekDayIndex(day);
        !!this.laborDays.find((i) => i === todaysLaborDayIndex) &&
          monthsLaborDays.push(day);
      }
      return monthsLaborDays;
    }
  
    changeNewDate(newDate) {
      this.newDate = newDate;
    }
  
    checkCurrentMonthsLaborDays() {
      this.currentMonthsLaborDays = this.#getCurrentMonthsLaborDays();
    }
  }
  
  const getCurrentMonthsLaborDays = (customDate) => {
    const dateHandler = new DateHandler(customDate);
    dateHandler.checkCurrentMonthsLaborDays();
    return dateHandler.currentMonthsLaborDays;
  }
  
  export default function laborDays(customDate = new Date()) {
    console.log(customDate);
    return getCurrentMonthsLaborDays(customDate);
  }

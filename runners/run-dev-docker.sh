alias with_docker="sudo docker run -it --volume $(pwd):/app --workdir /app node:lts-alpine"
with_docker npm install &&\
with_docker /bin/sh runners/run-dev.sh